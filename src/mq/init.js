const amqp = require('amqplib')
const logger = require('../trace/logger')
const { setPubChannel } = require('./publish-message')
const { processMessage } = require('./process-message')
const { config } = require('../setup')
const InvalidConfig = require('../errors/invalid-config')
const NoLockError = require('../errors/no-lock-error')
const {
  setListeners, setMQStatus
} = require('./mq-resources')
const publisher = require('./publish-message')
const { getSemaphore } = require('../sqldb/db')

const DB_LOCK_TIMEOUT = 10

async function setupChannel(conn) {
  logger.debug('Setting channel for AMQP')
  await startPublisher(conn)
  await startWorker(conn)
}

async function assertAndBindQueue(ch, queueName, exchangeName) {
  await ch.assertQueue(queueName, { durable: true })
  await ch.bindQueue(queueName, exchangeName, queueName)
}

async function ensureIncidentQueues(ch, exchangeName) {
  try {
    await assertAndBindQueue(ch, 'FailureIncident', exchangeName)
    await assertAndBindQueue(ch, 'HardError', exchangeName)
    await assertAndBindQueue(ch, 'InvalidMessageStructure', exchangeName)
    await assertAndBindQueue(ch, 'InvalidConfig', exchangeName)
    await assertAndBindQueue(ch, 'InvalidMessageContent', exchangeName)
    await assertAndBindQueue(ch, 'UnknownEvent', exchangeName)
    await assertAndBindQueue(ch, 'ComponentError', exchangeName)
    await assertAndBindQueue(ch, 'alert', exchangeName)
    logger.info('Bound all incident queues')
  } catch (err) {
    logger.error('Failed to bind Incident queues %j', err)
  }
}

async function getExchange(ch, exchangeName, exchangeType, args) {
  logger.debug('Asserting for exchange %s', exchangeName)
  const exchange = await ch.assertExchange(exchangeName, exchangeType, args)
  return exchange
}

async function bindQueue(ch, exchangeName, routingKey, queueName) {
  logger.info('Subscribing MQ client => Exchange : %s, Routing Key : %s, Queue : %s', exchangeName, routingKey, queueName)
  const q = await ch.bindQueue(queueName, exchangeName, routingKey)
  return q
}

async function doConsumeMessage(msg) {
  try {
    const result = await processMessage(msg)
    logger.debug('Message consumed. result sent to %j', result)
    return result
  } catch (err) {
    const result = await publisher.sendIncident(msg, err.message, err.retryCount || 0, err)
    logger.error('Message consumpion failed %j', err)
    logger.info('Send an incident to %j ', result)
    throw new Error(JSON.stringify(result))
  }
}

async function consumeMessage(msg, queueName, enforceConsecutiveFlow) {
  logger.debug('I have message = %s ', msg)
  if (enforceConsecutiveFlow) {
    logger.info('Need a lock to process this message')
    let hasLock
    let semaphore
    try {
      semaphore = getSemaphore()
      hasLock = await semaphore.lock(`${queueName}-mq-lock`, DB_LOCK_TIMEOUT)
    } catch (err) {
      logger.info('Error acquiring lock ', err)
      throw new NoLockError('Error acquiring lock', err)
    }
    if (hasLock) {
      logger.info('Lock acquired on ', `${queueName}-mq-lock`)
      try {
        await doConsumeMessage(msg)
      } catch (err) {
        throw err
      } finally {
        await semaphore.unlock(`${queueName}-mq-lock`)
        logger.info('Lock released')
      }
    } else {
      logger.info('Lock not available ')
      throw new NoLockError('Lock not available', undefined)
    }
  } else {
    await doConsumeMessage(msg)
  }
}

async function startMq(mqListeners) {
  logger.debug('Initiating connection for Rabbit MQ Client')
  if (!config.rabbitmqUser || !config.rabbitmqPwd || !config.rabbitmqHost || !config.rabbitmqPort) {
    logger.error('All environment variables for rabbitmq not provided. Ensure RABBITMQ_USER, RABBITMQ_PWD, RABBITMQ_HOST, RABBITMQ_PORT is properly set.')
    throw new InvalidConfig('Missing configuration settings for MQ Client')
  }

  try {
    logger.debug('Attempting a connection with MQ')
    const conn = await amqp.connect(`amqp://${config.rabbitmqUser}:${config.rabbitmqPwd}@${config.rabbitmqHost}:${config.rabbitmqPort}?heartbeat=${config.messageQueueHeartbeat}`, {})
    conn.on('error', (err) => {
      if (err.message !== 'Connection closing') {
        logger.error('[AMQP] conn error %s', err.message)
      }
    })
    conn.on('close', () => {
      logger.warn('[AMQP] reconnecting')
      const min = 1
      const rand = Math.floor((Math.random() * 10) + min) // Generate Random number between 1 - 10
      setMQStatus({
        isActive: false
      })
      return setTimeout(startMq, rand * 1000)
    })

    logger.info('[AMQP] connected')
    setMQStatus({
      isActive: true
    })
    setListeners(mqListeners)
    await setupChannel(conn)
    return true
  } catch (err) {
    logger.error('[AMQP] %s', err.message)
    const min = 1
    const rand = Math.floor((Math.random() * 10) + min) // Generate Random number between 1 - 10
    return setTimeout(startMq, rand * 1000)
  }
}

async function startPublisher(conn) {
  try {
    const ch = await conn.createConfirmChannel()
    ch.on('error', err => logger.error('[AMQP] channel error : %j', err.message))
    ch.on('close', () => logger.info('[AMQP] channel closed'))
    setPubChannel(ch)
    await ch.assertExchange(config.incidentExchange, 'topic', { durable: true })
    await ensureIncidentQueues(ch, config.incidentExchange)
    logger.info('Verified connection to %s exchange and asserted all incident queues for publishing incidents and alerts', config.incidentExchange)
  } catch (err) {
    logger.error('[AMQP] : some error has occurred %j', err)
  }
}

// A worker that acks messages only if processed succesfully
async function startWorker(conn) {
  try {
    const ch = await conn.createConfirmChannel()
    ch.on('error', (err) => {
      logger.error('[AMQP] channel error : %s', err.message)
    })
    ch.on('close', () => {
      logger.info('[AMQP] channel closed')
    })

    // TODO : See if this can be set per queue (exchnage-routingKey Level)
    if (config.concurrencyLimitPerWorker) {
      logger.warn('Setting a prefetch limit of %d on %s', config.concurrencyLimitPerWorker, config.selfQueueName)
      ch.prefetch(config.concurrencyLimitPerWorker, true)
    }
    const q = await ch.assertQueue(config.selfQueueName, { durable: true })
    ch.consume(q.queue, async (msg) => {
      try {
        await consumeMessage(msg, q.queue, config.enforceConsecutiveFlow)
        ch.ack(msg)
        return
      } catch (err) {
        if (err instanceof NoLockError) {
          if (err.error) {
            logger.warn('Lock failed error. Nacking the message with requeue')
          } else {
            logger.warn('Could not get lock. Nacking the message with requeue')
          }
          // TODO : A retry with delay can be considered here
          ch.nack(msg, false, true)
        } else {
          ch.nack(msg, false, false)
        }
      }
    }, { noAck: false })

    const exchangePromises = config.exchangesToSubscribe.map(async (arr) => {
      const exchangePromise = await getExchange(ch, arr.name, 'topic', { durable: true })
      return exchangePromise
    })
    await Promise.all(exchangePromises)

    const delayedExchangePromises = config.exchangesToSubscribe.map(async (arr) => {
      const exchangePromise = await getExchange(ch, `${arr.name}-delayed`, 'x-delayed-message', {
        durable: true,
        arguments: { 'x-delayed-type': 'topic' }
      })
      return exchangePromise
    })
    await Promise.all(delayedExchangePromises)

    const queuePromises = config.exchangesToSubscribe.map((arr) => {
      arr.keys.map(async (routingKey) => {
        const queuePromise = await bindQueue(ch, arr.name, routingKey, q.queue)
        return queuePromise
      })
    })
    await Promise.all(queuePromises)

    const delayedQueuePromises = config.exchangesToSubscribe.map((arr) => {
      arr.keys.map(async (routingKey) => {
        const queuePromise = await bindQueue(ch, `${arr.name}-delayed`, routingKey, q.queue)
        return queuePromise
      })
    })
    await Promise.all(delayedQueuePromises)

    logger.info('Worker is started and ready to consume messages')
    return
  } catch (err) {
    logger.error('[AMQP] : some error has occurred %s', err)
  }
}

module.exports = {
  startMq,
  consumeMessage
}
