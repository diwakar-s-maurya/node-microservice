let mqListeners = {}
let mqStatus = {}

function getListener(eventName) {
  return mqListeners[eventName]
}

function setListeners(listeners) {
  mqListeners = listeners
}

function setMQStatus(status) {
  mqStatus = status
}

function getMQStatus() {
  return mqStatus
}

module.exports = {
  getListener,
  setListeners,
  setMQStatus,
  getMQStatus
}
