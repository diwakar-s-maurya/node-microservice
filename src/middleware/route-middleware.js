const logger = require('../trace/logger')
const fs = require('fs')
const path = require('path')
const Joi = require('joi')

const routeSchema = Joi.object().keys({
  path: Joi.string().required(),
  method: Joi.string().required().valid('get', 'post', 'put', 'delete').insensitive(),
  handler: Joi.func().required(),
  config: Joi.object().required()
})

function addPathToRouter(route, router) {
  Joi.validate(route, routeSchema, (err) => {
    if (err) {
      logger.warn('Not a valid path object %s', err)
      return false
    }
    logger.info('Added Path for %s - %s', route.method.toUpperCase(), route.path)
    router[route.method.toLowerCase()](route.path, route.handler).describe(route.config)
  })
}

const routeCollection = (router, routeFolderPath) => {
  try {
    logger.info('Adding application Routes....... Start')
    fs.readdirSync(routeFolderPath).filter(file => (file.indexOf('.') !== 0) && (file.slice(-3) === '.js')).forEach((file) => {
      // Disabling dynamic require because we need to get all route files from service
      const routeCollection = require(path.join(routeFolderPath, file)); // eslint-disable-line global-require,import/no-dynamic-require
      if (Array.isArray(routeCollection)) {
        routeCollection.map((route) => {
          addPathToRouter(route, router)
        })
      } else if (typeof (routeCollection) === 'object') {
        addPathToRouter(routeCollection, router)
      }
    })
    logger.info('Added application Routes..... Done')
    return router
  } catch (err) {
    logger.error('Please fix issues in application Routes...', err)
  }
}

const routeMiddleware = (app, router, routeFolderPath) => {
  try {
    logger.debug('adding routes middleware')
    const routes = routeCollection(router, routeFolderPath)
    app.use('/', routes)
  } catch (err) {
    logger.error('Failed to run routes.. %s ', err)
    throw err
  }
}

module.exports = {
  routeMiddleware
}
