/*
 * This file exposes api function consumed by individual services whenver api calling need arises
 * Api function expect apiOptions (for overiding default config) and callback
*/
const { config } = require('../setup')
const request = require('../utils/request-utils')
const logger = require('../trace/logger')

function addDefaultApiConfig(apiOptions) {
  const defaultOptions = {
    url: '',
    query: {},
    method: 'GET',
    data: {},
    intercept: true, // by default api saga will intercept all requests for generic errors
    timeout: parseInt(config.API_TIMEOUT, 10),
    maxRetries: parseInt(config.API_MAX_RETRIES, 10),
    retryCount: 0,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  }
  const headers = { ...defaultOptions.headers, ...apiOptions.headers }
  return {
    ...defaultOptions,
    ...apiOptions,
    headers
  }
}

async function api(apiOptions) {
  const apiConfig = addDefaultApiConfig(apiOptions)

  try {
    return await request(apiConfig)
  } catch (err) {
    logger.debug('Calling Api - Config %s', JSON.stringify(apiConfig))
    logger.error('Error in API request %s', err)
    err.status = err.response.status
    err.data = err.response.data
    throw err
  }
}

module.exports = {
  addDefaultApiConfig,
  api
}
