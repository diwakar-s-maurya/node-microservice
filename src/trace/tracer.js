// Creating tracer for zipkin
const { Tracer } = require('zipkin')
const CLSContext = require('zipkin-context-cls-hooked')

const setUpZipkinRecorder = require('./../middleware/recorder')
const { config } = require('../setup')

let tracer

function setTracer() {
  const ctxImpl = new CLSContext()
  const recorder = setUpZipkinRecorder()
  tracer = new Tracer({ ctxImpl, recorder, localServiceName: config.microserviceName })
}

function getTracer() {
  return tracer
}

module.exports = {
  getTracer,
  setTracer
}
