module.exports = class HardError extends Error {
  constructor(message, error) {
    super(message)
    this.name = this.constructor.name
    this.error = error
    Error.captureStackTrace(this, this.constructor)
  }
}
