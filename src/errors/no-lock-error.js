module.exports = class NoLockError extends Error {
  constructor(message, error) {
    super(message)
    this.name = this.constructor.name
    this.error = error
    Error.captureStackTrace(this, this.constructor)
  }
}
