module.exports = class FailureIncident extends Error {
  constructor(message, error, retryCount) {
    super(message)
    this.name = this.constructor.name
    this.error = error
    this.retryCount = retryCount
    Error.captureStackTrace(this, this.constructor)
  }
}
