module.exports = class SoftError extends Error {
  constructor(message, retryAfter) {
    super(message)
    this.name = this.constructor.name
    this.delay = retryAfter * 1000 || 1000
    Error.captureStackTrace(this, this.constructor)
  }
}
