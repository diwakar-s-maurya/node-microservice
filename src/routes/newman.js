const express = require('express')
const swagger = require('swagger-spec-express')

const router = express.Router()
swagger.swaggerize(router)

router.post('/', (req, res) => {
  res.json(req.body)
}).describe({
  tags: ['internal'],
  responses: {
    200: {
      description: 'run newman test cases'
    }
  }
})

module.exports = router
