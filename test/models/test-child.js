
module.exports = (sequelize, DataTypes) => {
  const TestChild = sequelize.define('TestChild', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    parentId: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    classMethods: {
      associate(models) {
        // associations can be defined here
        TestChild.belongsTo(models.TestModel, { as: 'parentId' })
      }
    }
  })
  return TestChild
}
