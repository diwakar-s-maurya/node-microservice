
module.exports = (sequelize, DataTypes) => {
  const TestModel = sequelize.define('TestModel', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING
  }, {
    classMethods: {
      associate(models) {
        TestModel.hasMany(models.TestChild)
        // associations can be defined here
      }
    }
  })
  return TestModel
}
