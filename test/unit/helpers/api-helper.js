const cls = require('cls-hooked')

const namespace = cls.getNamespace('default')

function getRandomDataSuccess() {
  const headers = {
    'X-Transactionid': namespace.get('transactionid')
  }
  return {
    url: 'https://randomuser.me/api/',
    method: 'GET',
    maxRetries: 1,
    headers
  }
}

function getRandomDataErr() {
  const headers = {
    'X-Transactionid': namespace.get('transactionid')
  }
  return {
    url: 'https://randomuser.me/api/use',
    method: 'GET',
    maxRetries: 1,
    headers
  }
}

module.exports = {
  getRandomDataSuccess,
  getRandomDataErr
}
