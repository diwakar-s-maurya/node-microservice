const { startApp } = require('./helpers/test-server-setup')
const {
  getTracerClient,
  getCacheClient,
  getSqlDBClient,
  getMqClient,
  getMiddleware,
  getApiClient,
  getUtils,
  errors
} = require('../../index')

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)
const expect = chai.expect

const { serviceSetup, mqListeners } = require('../config/service-setup')
const InvalidConfig = require('../../src/errors/invalid-config')

describe('When we try to bootstrap an apifie service, with correct config setup', () => {
  describe('with all dependent components running at designated address', () => {
    it('with all required config keys are available and correct', async () => {
      const _setup = JSON.parse(JSON.stringify(serviceSetup))
      const apifiedApp = await startApp(_setup, mqListeners)
      expect(apifiedApp, 'App is not undefined').not.to.be.an('undefined')
      expect(getCacheClient, 'Cache client is not undefined').to.not.throw()
      expect(getSqlDBClient, 'DB client is not undefined').to.not.throw()
      expect(getMqClient, 'MQ client is not undefined').to.not.throw()
      expect(getApiClient, 'Api middleware components is not undefined').to.not.throw()
      expect(getMiddleware, 'Routes Middleware components is not undefined').to.not.throw()
      expect(getTracerClient, 'Tracer is not undefined').to.not.throw()
      expect(getUtils, 'Utils is not undefined').to.not.throw()
    })

    it('with cache not enabled in config', async () => {
      const _setup = JSON.parse(JSON.stringify(serviceSetup))
      _setup.cache.enabled = false
      const apifiedApp = await startApp(_setup, mqListeners)
      expect(apifiedApp, 'App is not undefined').not.to.be.an('undefined')
      expect(getCacheClient, 'Cache client is undefined').to.throw(errors.ComponentError)
      expect(getSqlDBClient, 'DB client is not undefined').to.not.throw()
      expect(getMqClient, 'MQ client is not undefined').to.not.throw()
      expect(getApiClient, 'Api middleware components is not undefined').to.not.throw()
      expect(getMiddleware, 'Routes Middleware components is not undefined').to.not.throw()
      expect(getTracerClient, 'Tracer is not undefined').to.not.throw()
      expect(getUtils, 'Utils is not undefined').to.not.throw()
    })

    it('and cache and db not enabled in config', async () => {
      const _setup = JSON.parse(JSON.stringify(serviceSetup))
      _setup.cache.enabled = false
      _setup.db.enabled = false
      const apifiedApp = await startApp(_setup, mqListeners)
      expect(apifiedApp, 'App is not undefined').not.to.be.an('undefined')
      expect(getCacheClient, 'Cache client is undefined').to.throw(errors.ComponentError)
      expect(getSqlDBClient, 'DB client is undefined').to.throw(errors.ComponentError)
      expect(getMqClient, 'MQ client is not undefined').to.not.throw()
      expect(getApiClient, 'Api middleware components is not undefined').to.not.throw()
      expect(getMiddleware, 'Routes Middleware components is not undefined').to.not.throw()
      expect(getTracerClient, 'Tracer is not undefined').to.not.throw()
      expect(getUtils, 'Utils is not undefined').to.not.throw()
    })

    it('and mq not enabled in config', async () => {
      const _setup = JSON.parse(JSON.stringify(serviceSetup))
      _setup.mq.enabled = false
      const apifiedApp = await startApp(_setup, mqListeners)
      expect(apifiedApp, 'App is not undefined').not.to.be.an('undefined')
      expect(getCacheClient, 'Cache client is not undefined').to.not.throw()
      expect(getSqlDBClient, 'DB client is not undefined').to.not.throw()
      expect(getMqClient, 'MQ client is undefined').to.throw(errors.ComponentError)
      expect(getApiClient, 'Api middleware components is not undefined').to.not.throw()
      expect(getMiddleware, 'Routes Middleware components is not undefined').to.not.throw()
      expect(getTracerClient, 'Tracer is not undefined').to.not.throw()
      expect(getUtils, 'Utils is not undefined').to.not.throw()
    })

    it('and trace not enabled in config', async () => {
      const _setup = JSON.parse(JSON.stringify(serviceSetup))
      _setup.trace.enabled = false
      const apifiedApp = await startApp(_setup, mqListeners)
      expect(apifiedApp, 'App is not undefined').not.to.be.an('undefined')
      expect(getCacheClient, 'Cache client is not undefined').to.not.throw()
      expect(getSqlDBClient, 'DB client is not undefined').to.not.throw()
      expect(getMqClient, 'MQ client is not undefined').to.not.throw()
      expect(getApiClient, 'Api middleware components is not undefined').to.not.throw()
      expect(getMiddleware, 'Routes Middleware components is not undefined').to.not.throw()
      expect(getTracerClient, 'Tracer is undefined').to.throw(errors.ComponentError)
      expect(getUtils, 'Utils is not undefined').to.not.throw()
    })

    it('and api middleware not enabled in config', async () => {
      const _setup = JSON.parse(JSON.stringify(serviceSetup))
      _setup.api.enabled = false
      _setup.routes.enabled = false
      const apifiedApp = await startApp(_setup, mqListeners)
      expect(apifiedApp, 'App is not undefined').not.to.be.an('undefined')
      expect(getCacheClient, 'Cache client is not undefined').to.not.throw()
      expect(getSqlDBClient, 'DB client is not undefined').to.not.throw()
      expect(getMqClient, 'MQ client is not undefined').to.not.throw()
      expect(getApiClient, 'Api middleware components is undefined').to.throw(errors.ComponentError)
      expect(getMiddleware, 'Routes Middleware components is undefined').to.throw(errors.ComponentError)
      expect(getTracerClient, 'Tracer is not undefined').to.not.throw()
      expect(getUtils, 'Utils is not undefined').to.not.throw()
    })
  })
})

describe('When we try to bootstrap an apifie service', () => {
  describe('with some missing config, like', () => {
    it('cache enabled but missing redis host and port', async () => {
      const _setup = JSON.parse(JSON.stringify(serviceSetup))
      _setup.cache.redisPort = undefined
      _setup.cache.redisHost = undefined
      return expect(startApp(_setup, mqListeners)).to.eventually.be.rejectedWith('Missing configuration settings for connection Redis Client')
    })

    it('mq enabled but missing mq username', async () => {
      const _setup = JSON.parse(JSON.stringify(serviceSetup))
      _setup.mq.rabbitmqUser = undefined
      return expect(startApp(_setup, mqListeners)).to.eventually.be.rejectedWith('Missing configuration settings for MQ Client')
    })

    it('db enabled but wrong env', async () => {
      const _setup = JSON.parse(JSON.stringify(serviceSetup))
      _setup.env = 'xxx'
      return expect(startApp(_setup, mqListeners)).to.eventually.be.rejectedWith(InvalidConfig)
    })

    it('trace enabled but missing transaction id header key', async () => {
      const _setup = JSON.parse(JSON.stringify(serviceSetup))
      _setup.trace.transactionIdHeaderKey = undefined
      return expect(startApp(_setup, mqListeners)).to.eventually.be.rejectedWith('Missing configuration settings for setup of tracer')
    })
  })
})
