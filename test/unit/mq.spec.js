const { startApp } = require('./helpers/test-server-setup')

const amqp = require('amqplib/callback_api')
const chai = require('chai')
const spies = require('chai-spies')
const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)
chai.use(spies)
const expect = chai.expect

const { logger } = require('../../index')
const { serviceSetup, mqListeners } = require('../config/service-setup')
const FailureIncident = require('../../src/errors/failure-Incident')
const UnknownEvent = require('../../src/errors/unknown-event-error')
const NoLockError = require('../../src/errors/no-lock-error')
const HardError = require('../../src/errors/hard-error')
const { processMessage } = require('../../src/mq/process-message')
const { consumeMessage } = require('../../src/mq/init')
const { getSemaphore } = require('../../src/sqldb/db')
const { getMqClient } = require('../../index')

const sampleMessageForOthers = {
  fields: {
    consumerTag: 'amq.ctag-2xM8BRvR67f21DVhFaXqAA',
    deliveryTag: 1,
    redelivered: false,
    exchange: 'module-test-app-1.0.0',
    routingKey: 'some-key'
  },
  properties: { headers: {}, deliveryMode: 1 },
  content: {
    event: 'some_event',
    metadata: {},
    data: {
      msg: 'some-message'
    }
  }
}

const myMessageWithoutHeader = {
  fields: {
    consumerTag: 'amq.ctag-VIpy_YzS5nTs2BRzhTvcIQ',
    deliveryTag: 1,
    redelivered: false,
    exchange: 'module-test-app-1.0.0',
    routingKey: 'notify-module-test-app'
  },
  properties: { headers: {}, deliveryMode: 1 },
  content: '{"event": "TEST_MESSAGE","data": {"someKey": "someValue"},"metadata" : {"appKey": "test-app-key","appContext": "test-app-context"}}'
}
const myMessageWithBadContentJson = {
  fields: {
    consumerTag: 'amq.ctag-N17x-EIrlrs0k-dp6NpyoA',
    deliveryTag: 1,
    redelivered: false,
    exchange: 'module-test-app-1.0.0',
    routingKey: 'notify-module-test-app'
  },
  properties: { headers: { msgType: 'REQUEST' }, deliveryMode: 1 },
  content: '{"event": "TEST_MESSAGE,"data": {"someKey": "someValue"},"metadata" : {"appKey": "test-app-key","appContext": "test-app-context"}}'
}
const myMessageBadContentStructure = {
  fields: {
    consumerTag: 'amq.ctag-8SbA-jwwpFIm6rTovonPQw',
    deliveryTag: 1,
    redelivered: false,
    exchange: 'module-test-app-1.0.0',
    routingKey: 'notify-module-test-app'
  },
  properties: { headers: { msgType: 'REQUEST' }, deliveryMode: 1 },
  content: '{"someKey": "someValue"}'
}
const myMessageWithUnknownEvent = {
  fields: {
    consumerTag: 'amq.ctag-N17x-EIrlrs0k-dp6NpyoA',
    deliveryTag: 1,
    redelivered: false,
    exchange: 'module-test-app-1.0.0',
    routingKey: 'notify-module-test-app'
  },
  properties: { headers: { msgType: 'REQUEST' }, deliveryMode: 1 },
  content: '{"event": "TEST_MESSAGE_XXX","data": {"someKey": "someValue"},"metadata" : {"appKey": "test-app-key","appContext": "test-app-context"}}'
}
const myMessageForSoftError = {
  fields: {
    consumerTag: 'amq.ctag-N17x-EIrlrs0k-dp6NpyoA',
    deliveryTag: 1,
    redelivered: false,
    exchange: 'module-test-app-1.0.0',
    routingKey: 'notify-module-test-app'
  },
  properties: { headers: { msgType: 'REQUEST' }, deliveryMode: 1 },
  content: '{"event": "TEST_MESSAGE","data": {"someKey": "someValue"},"metadata" : {"appKey": "SoftError","appContext": "test-app-context"}}'
}
const myMessageForHardError = {
  fields: {
    consumerTag: 'amq.ctag-N17x-EIrlrs0k-dp6NpyoA',
    deliveryTag: 1,
    redelivered: false,
    exchange: 'module-test-app-1.0.0',
    routingKey: 'notify-module-test-app'
  },
  properties: { headers: { msgType: 'REQUEST' }, deliveryMode: 1 },
  content: '{"event": "TEST_MESSAGE","data": {"someKey": "someValue"},"metadata" : {"appKey": "HardError","appContext": "test-app-context"}}'
}
const myMessageForFailureIncident = {
  fields: {
    consumerTag: 'amq.ctag-N17x-EIrlrs0k-dp6NpyoA',
    deliveryTag: 1,
    redelivered: false,
    exchange: 'module-test-app-1.0.0',
    routingKey: 'notify-module-test-app'
  },
  properties: { headers: { msgType: 'REQUEST' }, deliveryMode: 1 },
  content: '{"event": "TEST_MESSAGE","data": {"someKey": "someValue"},"metadata" : {"appKey": "FailureIncident","appContext": "test-app-context"}}'
}
const myMessageForProcessing = {
  fields: {
    consumerTag: 'amq.ctag-N17x-EIrlrs0k-dp6NpyoA',
    deliveryTag: 1,
    redelivered: false,
    exchange: 'module-test-app-1.0.0',
    routingKey: 'notify-module-test-app'
  },
  properties: { headers: { msgType: 'REQUEST' }, deliveryMode: 1 },
  content: '{"event": "TEST_MESSAGE","data": {"someKey": "someValue"},"metadata" : {"appKey": "test-app-key","appContext": "test-app-context"}}'
}

const myMessageForProcessingWithReturnAddress = {
  fields: {
    consumerTag: 'amq.ctag-N17x-EIrlrs0k-dp6NpyoA',
    deliveryTag: 1,
    redelivered: false,
    exchange: 'module-test-app-1.0.0',
    routingKey: 'notify-module-test-app'
  },
  properties: { headers: { msgType: 'REQUEST' }, deliveryMode: 1 },
  content: '{"event": "TEST_MESSAGE","data": {"someKey": "someValue"},"metadata" : {"replyToExchange":"amq.topic", "replyToRoutingKey":"xkey", "replyAsEvent":"xevent", "appKey": "test-app-key","appContext": "test-app-context"}}'
}

async function deleteTestExchanges() {
  await
  amqp.connect(`amqp://${serviceSetup.mq.rabbitmqUser}:${serviceSetup.mq.rabbitmqPwd}@${serviceSetup.mq.rabbitmqHost}:${serviceSetup.mq.rabbitmqPort}?heartbeat=${serviceSetup.mq.messageQueueHeartbeat}`, {}, async (err, conn) => {
    if (err) {
      logger.error('[TEST AMQP]', err.message)
      throw err
    }
    conn.on('error', (err) => {
      if (err.message !== 'Connection closing') {
        logger.error('[TEST AMQP] conn error', err.message)
      }
    })
    conn.on('close', () => {
      logger.warn('[TEST AMQP] closed')
    })
    logger.info('[TEST AMQP] connected')
    await conn.createConfirmChannel(async (err, ch) => {
      if (err) {
        logger.error('[TEST AMQP] cannot delete test exchange ', err)
        return false
      }
      conn.on('error', (err) => {
        if (err.message !== 'Connection closing') {
          logger.error('[TEST AMQP] conn error', err.message)
        }
      })
      conn.on('close', () => {
        logger.warn('[TEST AMQP] closed')
      })
      logger.info('[TEST AMQP] connected')
      await conn.createConfirmChannel(async (err, ch) => {
        if (err) {
          logger.error('[TEST AMQP] cannot delete test exchange ', err)
          return false
        }
        logger.info('Deleting test exchange')
        await ch.deleteExchange(`${serviceSetup.mq.selfQueueName}-XXX`)
        await ch.purgeQueue('incident-q')
        logger.info('Deleted messages from Incident Queue')
      })
      return true
    })
    return true
  })
}

describe('When we apifie a service with consequent flow on mq, but no db', () => {
  const _setup = JSON.parse(JSON.stringify(serviceSetup))
  _setup.db.enabled = false
  _setup.mq.enforceConsecutiveFlow = true
  it('fails to Bootstrap ', async () => expect(startApp(_setup, mqListeners)).to.eventually.be.rejectedWith('DB must be enabled to enforceConsecutiveFlow'))
})

describe('When we apifie a service with consequent flow on mq, and a db', () => {
  const _setup = JSON.parse(JSON.stringify(serviceSetup))
  _setup.db.enabled = true
  _setup.mq.enforceConsecutiveFlow = true

  it('It gets bootstrapped ', async () => expect(startApp(_setup, mqListeners)).to.eventually.be.fulfilled)
})

describe('When we apifie a service with consequent flow', () => {
  const _setup = JSON.parse(JSON.stringify(serviceSetup))
  _setup.mq.enforceConsecutiveFlow = true
  before(async () => {
    await startApp(_setup, mqListeners)
    await deleteTestExchanges()
  })
  after(async () => {
    await deleteTestExchanges()
  })

  describe('And lock is available', () => {
    it('We can process a message without any issues ', async () => expect(consumeMessage(myMessageForProcessing, 'test-q', true)).to.eventually.be.fulfilled)
  })

  describe('And lock is not available', () => {
    let semaphore
    before(async () => {
      semaphore = await getSemaphore()
      await semaphore.lock('test-q-mq-lock', 5)
    })

    after(async () => {
      await semaphore.unlock('test-q-mq-lock')
    })

    it('It fails with No Lock Error', async () => expect(consumeMessage(myMessageForProcessing, 'test-q', true)).to.eventually.be.rejectedWith(NoLockError))
  })
})

describe('When we apifie a service with mq enabled and correct config', () => {
  let apifiedApp

  before(async () => {
    apifiedApp = await startApp(serviceSetup, mqListeners)
    await deleteTestExchanges()
  })

  after(async () => {
    await deleteTestExchanges()
  })

  describe('We are able to send different type of messages', () => {
    it('we get a mq client and required functions ', async () => {
      expect(getMqClient, 'mq is not undefined').to.not.throw()
      expect(getMqClient().sendRequest, 'has a funtion to send a request message').to.be.a('function')
      expect(getMqClient().sendResponse, 'has a funtion to send a response message').to.be.a('function')
      expect(getMqClient().sendRetryRequest, 'has a funtion to send a retry message').to.be.a('function')
      expect(getMqClient().sendIncident, 'has a funtion to send a incident alert message').to.be.a('function')
    })

    it('we can send a (request) message to an existing exchange', async () => expect(getMqClient().sendRequest(serviceSetup.mq.selfQueueName, `notify-${serviceSetup.microserviceName}`, 'TEST_MESSAGE', { a: 'x' }, { abc: 'xyz' })).to.eventually.not.be.rejected)

    // TODO: This test should fail after we fix the MQ publish exchange check issue
    xit('we can send a message to a new exchange', async () => expect(apifiedApp.mq.sendRequest(`${serviceSetup.mq.selfQueueName}-XXX`, `notify-${serviceSetup.microserviceName}-XXX`, 'SEND_REQ', { a: 'x' }, { abc: 'xyz' })).to.eventually.not.be.rejected)

    it('we can send a response message', async () => expect(getMqClient().sendResponse(serviceSetup.mq.selfQueueName, 'unit-test', 'SEND_REQ', { a: 'x' }, { abc: 'xyz' })).to.eventually.not.be.rejected)

    it('we can send a retry message', async () => expect(getMqClient().sendRetryRequest(sampleMessageForOthers, 1, 1000)).to.eventually.not.be.rejected)
  })

  describe('and receive a request message on our queue', () => {
    it('rejects the message with missing headers ', async () => {
      expect(processMessage(myMessageWithoutHeader)).to.eventually.be.rejectedWith('Message is missing properties or headers')
    })

    it('rejects the message with bad json ', async () => {
      expect(processMessage(myMessageWithBadContentJson)).to.eventually.be.rejectedWith('Invalid json structure in message content')
    })

    it('rejects the message that does not comply with apifie message schema ', async () => {
      expect(processMessage(myMessageBadContentStructure)).to.eventually.be.rejectedWith('Message does not comply with apifie schema')
    })

    it('rejects the message if its has an unknown event ', async () => {
      expect(processMessage(myMessageWithUnknownEvent)).to.eventually.be.rejectedWith(UnknownEvent)
    })

    it('throws an error if message processing fails with server error type of issues ', async () => {
      expect(processMessage(myMessageForHardError)).to.eventually.be.rejectedWith(HardError)
    })

    it('not throw an error if message processing fails with a retry request ', async () => expect(processMessage(myMessageForSoftError)).to.eventually.be.fulfilled)

    it('throws an error if message processing fails with an incident alert ', async () => {
      expect(processMessage(myMessageForFailureIncident)).to.eventually.be.rejectedWith(FailureIncident)
    })

    it('not throw an error if message processing is successfull ', async () => expect(processMessage(myMessageForProcessing)).to.eventually.be.fulfilled)
  })

  describe('and send a response after processing a (valid) message', () => {
    it('Response is sent on processor exchange if there is no return address ', async () => {
      const result = await processMessage(myMessageForProcessing)
      expect(result.exchange, 'Reply to exchange is correct').to.equal(myMessageForProcessing.fields.exchange)
      expect(result.routingKey, 'Reply to routing key is correct').to.equal(`${myMessageForProcessing.fields.routingKey}-res`)
      expect(result.event, 'Reply to event is correct').to.equal('TEST_MESSAGE')
    })

    it('Response is sent on designated exchange if there is a return address ', async () => {
      const result = await processMessage(myMessageForProcessingWithReturnAddress)
      expect(result.exchange, 'Reply to exchange is correct').to.equal('amq.topic')
      expect(result.routingKey, 'Reply to routing key is correct').to.equal('xkey')
      expect(result.event, 'Reply to event is correct').to.equal('xevent')
    })

    it('Messsage is sent back on processor exchange in case of a retry  ', async () => {
      const result = await processMessage(myMessageForSoftError)
      expect(result.exchange, 'Reply to exchange is correct').to.equal('module-test-app-1.0.0-delayed')
      expect(result.routingKey, 'Reply to routing key is correct').to.equal('notify-module-test-app')
      expect(result.event, 'Reply to event is correct').to.equal('TEST_MESSAGE')
    })
  })

  describe('in case response is a failure', () => {
    it('Messsage is sent to incident exchange with InvalidMessageStructure event in case of missing header  ', async () => {
      try {
        await consumeMessage(myMessageWithoutHeader)
      } catch (err) {
        const result = JSON.parse(err.message)
        expect(result.exchange, 'is sent to incident exchange').to.equal('incident-1.0.0')
        expect(result.routingKey, 'with a key = InvalidMessageStructure').to.equal('InvalidMessageStructure')
        expect(result.event, 'And an alert event').to.equal('alert')
      }
    })

    it('Message is sent to incident exchange with HardError event in case of a HardError  ', async () => {
      try {
        await consumeMessage(myMessageForHardError)
      } catch (err) {
        const result = JSON.parse(err.message)
        expect(result.exchange, 'is sent to incident exchange').to.equal('incident-1.0.0')
        expect(result.routingKey, 'with a key = HardError').to.equal('HardError')
        expect(result.event, 'And an HardError event').to.equal('alert')
      }
    })

    it('Messsage is sent to incident exchange with FailureIncident event in case of a FailureIncident  ', async () => {
      try {
        await consumeMessage(myMessageForFailureIncident)
      } catch (err) {
        const result = JSON.parse(err.message)
        expect(result.exchange, 'is sent to incident exchange').to.equal('incident-1.0.0')
        expect(result.routingKey, 'with a key = FailureIncident').to.equal('FailureIncident')
        expect(result.event, 'And an FailureIncident event').to.equal('alert')
      }
    })

    it('And incident alert sent has has required information  ', async () => {
      try {
        await consumeMessage(myMessageForFailureIncident)
      } catch (err) {
        const result = JSON.parse(err.message)
        expect(result.metadata.issue.message).to.equal('Sending FailureIncident')
        expect(result.metadata.issue.error).not.to.be.an('undefined')
        expect(result.metadata.issue.retryCount).to.equal(5)
        expect(result.metadata.target.exchange).to.equal('module-test-app-1.0.0')
        expect(result.metadata.target.routingKey).to.equal('notify-module-test-app')
        expect(result.metadata.target.event).to.equal('TEST_MESSAGE')
      }
    })
  })
})
