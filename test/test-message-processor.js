const logger = require('./../src/trace/logger')
const SoftError = require('./../src/errors/soft-error')
const HardError = require('./../src/errors/hard-error')
const FailureIncident = require('./../src/errors/failure-Incident')

/* Sample log message
{
  "event": "LOG_MESSAGE",
  "data": {
    "applicationId": "12345678"
  },
  "metadata" : {
    "appKey": "test-app-key",
    "appContext": "test-app-context"
  }
}
*/

async function processTestMessage(metadata, data, retryCount) {
  // Do your work here
  if (metadata.appKey === 'SoftError') {
    if (retryCount < 1) {
      logger.info('I am throwing soft error')
      throw new SoftError('Sending SoftError', 2)
    } else {
      logger.info('I am throwing hard error after %d retries', retryCount)
      throw new HardError('Sending HardError', new Error('xxx'))
    }
  } else if (metadata.appKey === 'HardError') {
    logger.info('I am throwing hard error')
    throw new HardError('Sending HardError', new Error('xxx'))
  } else if (metadata.appKey === 'FailureIncident') {
    logger.info('I am throwing incident')
    throw new FailureIncident('Sending FailureIncident', new Error('yyy'), 5)
  } else {
    logger.info('I have processed test message %j %j', metadata, data)
    return {
      result: 'ok',
      status: 200
    }
  }
}

module.exports = processTestMessage
