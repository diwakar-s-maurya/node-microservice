# node-microservice NPM Module

[![pipeline status](https://gitlab.com/apifie/nodems/node-microservice/badges/master/pipeline.svg)](https://gitlab.com/apifie/nodems/node-microservice/commits/master)
[![coverage report](https://gitlab.com/apifie/nodems/node-microservice/badges/master/coverage.svg)](https://gitlab.com/apifie/nodems/node-microservice/commits/master)

> Please refer to our [Project Wiki](https://gitlab.com/apifie/nodems/node-microservice/wikis/home) for a very detailed documentation and examples.


node-microservice is an aggregation of industry standard **best practices** and **design patterns** for creating micro-services using [Node.js](https://nodejs.org).  
It is a node module (npm) that gives a standard blueprint for creating new micro-services in Node.js. This blueprint attempts to bring in industry standard [design patterns](https://microservices.io/patterns/index.html) relevant and required for a micro-services based architecture. To implement these patterns, we typically need to integrate with a lot of tools and services from Infrastructure and Platform. This blueprint takes a opinionated view on selection of these tools and offers these integrations out-of-the-box. A list of these tools and services pre-integrated in node-microservice are as below

1. **Redis 4.0+** : It is used as a caching server.
2. **MySQL 5.6+** : It is used as a persistent data store [Database schema per service pattern](https://microservices.io/patterns/data/database-per-service.html).
3. **Rabbit MQ 3.6.12+** : It is used to support [Asynchronous inter-service communication pattern](https://microservices.io/patterns/communication-style/messaging.html).
4. **Winston 2.4+ and winston-logstash 0.4+** : It is used to support upstream [aggregation of application logs](https://microservices.io/patterns/observability/application-logging.html) to Platform level monitoring systems like ELK, Splunk etc.
5. **Zipkin** : Used to support [traceability of requests](https://microservices.io/patterns/observability/distributed-tracing.html) across different services, including async flows
6. **Axios 0.18+** : To manage HTTP requests and responses
7. **Swagger specs** : To automatically swagger specifications for all HTTP routes published by your service. This is further helpful in upstream integration with [Application gateways](https://microservices.io/patterns/apigateway.html)

## What it does (features-list)
> Read it on our [detailed Wiki](https://gitlab.com/apifie/nodems/node-microservice/wikis/feature-list).
In addition to above out-of-the-box integrations, this blueprint also provides in-built solutions for some of the most common and frustrating issues that developers have faced while working on micro-services architecture and its underlying complexities. Some of the key solutions offered out-of-box are listed below.

**Database (ORM)**
* Manage Database schema migrations
* Inject SSL certificates for DB connection (if required)
* Pre-Integrated with Sequelize
* Automated reconnection to DB server in case of intermittent connection failures

**Message Queue (Async communication)**

* Automated creation / binding to message exchanges and routing keys to service queues
* Scoping Message queues to specific service-name and version (to support multiple versions of services)
* Auto retry of failed messages on MQ (with or without a delay)
* Managing MQ failures by posting all incidents on a dedicated queue (for manual recover options)
* Automated reconnection to MQ server in case of intermittent connection failures

**Caching**
* Scoping of all cache keys to service-name and version
* Support for asynchronous read and write from cache
* Support for expiring keys after a set interval
* Automated reconnection to Redis server in case of intermittent connection failures

**Traceability and logging**
* Automatic addition of a unique transactionId to all incoming (HTTP and MQ) requests
* Chaining of this transactionId to all subsequent internal, inter-service calls (Both Sync and Async, including MQ interfaces)
* Injection of this transactionId in all application log messages
* Injection of a calculated ```responseTime``` header in all API (HTTP and MQ) responses
* Injection of serviceName and serviceVersion information in response headers
* Pushing all application logs to a logstash agent for upstream integration with tools like ELK, Splunk etc.
* Automated reconnection to logstash in case of intermittent connection failures

**HTTP request**
* Enforce standard REST routes
* Inject standard middleware components like ErrorHandlers, BodyParsers, application Routes
* Inject specific middleware components like express and zipkin
* Ensure compliance to specific headers for supporting traceability (See above)
* Automated generation of Swagger specification for all HTTP REST routes exposed by the service
* Automated generation of Postman scripts for all HTTP REST routes exposed by the service

**Testing and Quality Assurance**
* In-built support for [Chai](https://www.chaijs.com/), [Mocha](https://mochajs.org/) and [Proxyquire](https://github.com/thlorenz/proxyquire)
* A fully functional test harness, and helper function to kick start unit and integration test cases
* Generation of standard code coverage report
* Enforce compliance to standard coding practices (using ESLint)
* Generation of standard sonarqube reports

**Health check / Monitoring**
* Out-of-box HTTPS routes to support automated health monitoring in a clustered / cloud setup and upstream integration with an Alert system
* Ability to check health of all downstream components (Database, Cache, MQ etc) without getting in the service box (server / vm)

We maintain a very detailed documentation with a lot of examples and use cases. Please refer to to [node-microservice wiki page](https://gitlab.com/apifie/nodems/node-microservice/wikis/home)

## Installation
Node.js
node-microservice is available on npm. To install it, type:

```bash
$ npm install @apifie/node-microservice
```

## Usage
> Read it on our [detailed Wiki](https://gitlab.com/apifie/nodems/node-microservice/wikis/How%20to%20Start)

We need to create a micro-service that uses this module as its dependency.
The simplest way to start your new micro-service is to fork / clone one [our sample service](https://gitlab.com/apifie/nodems/examples/full-feature-micro-service-example) and start adding your business logic

At a very basic level, there are three steps.
1. Configure you service and define which components to use and what are different server end-points (like MySQL, redis etc.)
    * [Configuration guideline](https://gitlab.com/apifie/nodems/node-microservice/wikis/configuration-guidelines)
    * [External component setup](https://gitlab.com/apifie/nodems/node-microservice/wikis/how-to-manage-external-systems-and-components)
2. Import @apifie/node-microservice module in your service. Preferably, do this at the very start of your service (```index.js```)

  ```javascript
  const {
    logger,
    errors,
    serviceBootstrapper,
    router,
    swagger,
    getTracerClient,
    getCacheClient,
    getSqlDBClient,
    getMqClient,
    getMiddleware,
    getApiClient,
    getUtils,
    eventBus
  } = require('@apifie/node-microservice')
  ```

3. Call Service bootstrap helper function (async operation)

```javascript
async function bootstrapService (app) {
  logger.info('Requesting service bootstrap for your app')
  resgisterBootstrapperEvents()

  try {
    apifiedApp = await serviceBootstrapper(serviceSetup, mqListeners, app)
    return apifiedApp
  } catch (err) {
    logger.error('Failed in Bootstrapping your service %s ', err)
    throw err
  }
}
```

Once above steps are executed, consuming different capabilities is all about calling available methods on APIs exposed by this module. An example for cache would be as below

```javascript
await getCacheClient().setRedisKey('k1', 'v1')
const val = await getCacheClient().getRedisKey('k1')
```

### Testing the module
```bash
npm test
npm run test:integration
```

For a detailed documentation on test harness, bootstrapping and support for different types of testing (Unit, Integration, Flow etc.), you can refer to to [node-microservice wiki page](https://gitlab.com/apifie/nodems/node-microservice/wikis/testing-your-service)

## Component API Specification
Here is a quick reference to our detailed documentation to help and support you in using ```node-microservice```
* [X] **[What is ```node-microservice```](https://gitlab.com/apifie/nodems/node-microservice/wikis/home)**
* [X] **[Features and capabilities](https://gitlab.com/apifie/nodems/node-microservice/wikis/feature-list)**
* [X] **[`Configuration guideline`](https://gitlab.com/apifie/nodems/node-microservice/wikis/configuration-guidelines)**
* [X] **[External component setup](https://gitlab.com/apifie/nodems/node-microservice/wikis/how-to-manage-external-systems-and-components)**
* [X] **[Getting started guide](https://gitlab.com/apifie/nodems/node-microservice/wikis/How%20to%20Start)**
    * [X] [Manual install using npm](https://gitlab.com/apifie/nodems/node-microservice/wikis/create-a-new-micro-service-manually)
    * [X] [Start and adapt form a sample code](https://gitlab.com/apifie/nodems/node-microservice/wikis/creating-a-new-micro-service-from-sample-repo)
* [X] **[API and service Specification](https://gitlab.com/apifie/nodems/node-microservice/wikis/api-specification)**
    * [X] [Cache API](https://gitlab.com/apifie/nodems/node-microservice/wikis/cache-api-specification)
    * [X] [Message Queue API](https://gitlab.com/apifie/nodems/node-microservice/wikis/mq-api-specification)
    * [X] **[Database (ORM) Services](https://gitlab.com/apifie/nodems/node-microservice/wikis/database-orm-design)**
        * [X] [How to add a new model to database](https://gitlab.com/apifie/nodems/node-microservice/wikis/database-orm-design#how-to-add-new-model)
        * [X] [How to manage database migrations](https://gitlab.com/apifie/nodems/node-microservice/wikis/database-orm-design#how-to-manage-database-migrations-ddl)
    * [X] **[Middleware Router services](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification)**
        * [X] [Interceptors offered out-of-box by node-microservice](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification#route-interceptors-injected-automatically)
        * [X] [API Routes offered out-of-box by node-microservice](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification#internal-routes-for-monitoring-and-health-check)
        * [X] [How to add service specific middleware request / response interceptors](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification#how-to-add-custom-middleware)
        * [X] [How to add service specific api routes](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification#how-to-add-service-routes)
        * [X] [How to call external (HTTP / HTTPS) apis](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification#how-to-call-external-http-routes)
    * [X] [Utility services](https://gitlab.com/apifie/nodems/node-microservice/wikis/home)
        * [X] [How to call external (HTTP / HTTPS) apis](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification#how-to-call-external-http-routes)
        * [X] [Swagger Specs generation](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification#how-to-generate-swagger-specs-and-postman-collections)
        * [X] [Postman collection generation](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification#how-to-generate-swagger-specs-and-postman-collections)
    * [X] [Errors and Exceptions](https://gitlab.com/apifie/nodems/node-microservice/wikis/errors-specification)
    * [X] [Service Bootstrapper](https://gitlab.com/apifie/nodems/node-microservice/wikis/startup-flow-for-an-apified-microservice#servicebootstrapper-api)
* [X] **[Testing your service](https://gitlab.com/apifie/nodems/node-microservice/wikis/testing-your-service)**
    * [X] [Test helpers offered out-of-box by node-microservice](https://gitlab.com/apifie/nodems/node-microservice/wikis/testing-your-service#test-helpers)
    * [X] [How to write Unit test cases](https://gitlab.com/apifie/nodems/node-microservice/wikis/testing-your-service#writing-unit-test-cases)
    * [X] [How to write Integration test cases](https://gitlab.com/apifie/nodems/node-microservice/wikis/testing-your-service#writing-integration-test-cases)
* [X] **[How to guides](https://gitlab.com/apifie/nodems/node-microservice/wikis/home)**
    * [X] [How to use logger](https://gitlab.com/apifie/nodems/node-microservice/wikis/how-to-use-logger)
    * [X] [How to use tracer](https://gitlab.com/apifie/nodems/node-microservice/wikis/how-to-use-tracer)
    * [X] [How to add a new model to database](https://gitlab.com/apifie/nodems/node-microservice/wikis/database-orm-design#how-to-add-new-model)
    * [X] [How to manage database migrations](https://gitlab.com/apifie/nodems/node-microservice/wikis/database-orm-design#how-to-manage-database-migrations-ddl)
    * [X] [How to add service specific middleware request / response interceptors](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification#how-to-add-custom-middleware)
    * [X] [How to add service specific api routes](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification#how-to-add-service-routes)
    * [X] [How to call external (HTTP / HTTPS) apis](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification#how-to-call-external-http-routes)
    * [X] [How to generate Swagger Specs](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification#how-to-generate-swagger-specs-and-postman-collections)
    * [X] [How to generate Postman collection](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification#how-to-generate-swagger-specs-and-postman-collections)
* [X] **[Understanding technical design](https://gitlab.com/apifie/nodems/node-microservice/wikis/home)**
    * [X] [Understanding startup / bootstrapping flow of your service](https://gitlab.com/apifie/nodems/node-microservice/wikis/startup-flow-for-an-apified-microservice#sequence-diagram-for-service-startup-process)
    * [X] [Understanding Swagger Specs generation](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification#how-to-generate-swagger-specs-and-postman-collections)
    * [X] [Understanding Postman collection generation](https://gitlab.com/apifie/nodems/node-microservice/wikis/middleware-router-service-specification#how-to-generate-swagger-specs-and-postman-collections)
    * [X] [Understanding Message Queue Interface](https://gitlab.com/apifie/nodems/node-microservice/wikis/Message-Queue-Technical-design)
    * [X] [Understanding Database (ORM) module design](https://gitlab.com/apifie/nodems/node-microservice/wikis/database-orm-design)
* [X] **[Best practices and guidelines](https://gitlab.com/apifie/nodems/node-microservice/wikis/home)**
    * [X] [Recommended Project Structure for your micro-services](https://gitlab.com/apifie/nodems/node-microservice/wikis/recommended-project-folder-structure)
* [X] **[How to contribute](https://gitlab.com/apifie/nodems/node-microservice/wikis/how-to-contribute)**
* [X] **[How to report an Issue](https://gitlab.com/apifie/nodems/node-microservice/wikis/how-to-report-an-issue)**

## Meet the crew
[![@mail2gauravkumar](https://gitlab.com/apifie/node-microservice/wikis/uploads/cd86b8312c87129cda4a0114304c2f49/Me_Small.jpg)](https://www.linkedin.com/in/mail2gauravkumar/)
[![charu](https://gitlab.com/apifie/node-microservice/wikis/uploads/2a78a9bdfcca465b92c7ac966e6ab731/charu.jpg)](https://www.linkedin.com/in/priya-charu/)
[![diwakar](https://gitlab.com/apifie/node-microservice/wikis/uploads/d2e3734d2d33bd01c27743fdfdb1eb8d/diwakar.jpeg)](https://www.linkedin.com/in/diwakar-singh-maurya-11a209110/)
[![lalit](https://gitlab.com/apifie/node-microservice/wikis/uploads/ba0c8efeed07318deba689cec6c2c258/lalit.jpeg)](https://www.linkedin.com/in/rhombus/)
[![dave](https://gitlab.com/apifie/node-microservice/wikis/uploads/f227e3391cd25c02ae09e07ad13b25ea/dave.jpeg)](https://www.linkedin.com/in/dwmkerr/)
